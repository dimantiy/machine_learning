print '# Task 06. SVN'
print

import numpy as np
import pandas as pd
from sklearn.svm import SVC

data = pd.read_csv('../data/svm-data.csv', header=None, names=["C", "X", "Y"])
X = data[["X","Y"]]
y = data["C"]

clf = SVC(kernel='linear', C = 100000, random_state=241)
clf.fit(X, y)

# print clf.support_

sup = np.array(clf.support_) + 1

print '1. Support points'
print '  ',  ",".join([str(i) for i in sup])

out=open('../output/06/6.1.txt', 'w+')
print >>out, ",".join([str(i) for i in sup]),