print '# Task 02. Titanic'
print

import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier

# Data
data = pd.read_csv('../data/titanic.csv', index_col='PassengerId')
dataClear = data[pd.notnull(data['Age'])]

# Objects
#X = dataClear[['Pclass', 'Fare', 'Age', 'Sex']]
X = pd.DataFrame({
    'Pclass': dataClear['Pclass'],
    'Fare': dataClear['Fare'],
    'Age': dataClear['Age'],
    'Sex': dataClear['Sex'].apply(lambda x: 1 if x == 'male' else 0)
})

# Answers
Y = dataClear['Survived']

clf = DecisionTreeClassifier(random_state=241)
clf.fit(X, Y)

importances = clf.feature_importances_
fields = pd.DataFrame({
    'Name': ['Pclass', 'Fare', 'Age', 'Sex'],
    'Weight': importances 
})
mean = fields['Weight'].median()
result = fields['Name'][fields['Weight'] > mean].values

print '1. Importances'
print '  ', result[0], result[1],

out=open('../output/02/2.1.txt', 'w+')
print >>out, result[0], result[1],