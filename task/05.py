print '# Task 05. Perceptron'
print



import numpy as np
import pandas as pd
from sklearn.linear_model import Perceptron
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score

train = pd.read_csv('../data/perceptron-train.csv', header=None, names=["C", "X", "Y"])
test = pd.read_csv('../data/perceptron-test.csv', header=None, names=["C", "X", "Y"])

Ytrain = train["C"]
Ytest = test["C"]

Xtrain = train[["X","Y"]]
Xtest = test[["X","Y"]]

scaler = StandardScaler()

XtrainNorm = scaler.fit_transform(Xtrain)
XtestNorm = scaler.transform(Xtest)

clf = Perceptron(random_state=241)
clf.fit(Xtrain, Ytrain)
Ypred = clf.predict(Xtest)
acc = accuracy_score(Ytest, Ypred)

clfNorm = Perceptron(random_state=241)
clfNorm.fit(XtrainNorm, Ytrain)
YpredNorm = clfNorm.predict(XtestNorm)
accNorm = accuracy_score(Ytest, YpredNorm)

da = accNorm - acc

print '1. Accuracy'
print '  ', "%.3f" % acc, "%.3f" % accNorm, "%.3f" % da 

# print result

out=open('../output/05/5.1.txt', 'w+')
print >>out, "%.3f" % da,