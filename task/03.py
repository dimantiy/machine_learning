print '# Task 03. kNN'
print




from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import KFold
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import scale

import numpy as np
import pandas as pd
import sklearn as sk
# from sklearn.tree import DecisionTreeClassifier

data = pd.read_csv('../data/wine.csv', header=None, names=[
    "Class",
    "Alcohol",
    "Malic acid", 
    "Ash",
    "Alcalinity of ash", 
    "Magnesium",
    "Total phenols", 
    "Flavanoids",
    "Nonflavanoid phenols", 
    "Proanthocyanins",
    "Color intensity", 
    "Hue",
    "OD280/OD315 of diluted wines", 
    "Proline"
])
X = data[[
    "Alcohol",
    "Malic acid", 
    "Ash",
    "Alcalinity of ash", 
    "Magnesium",
    "Total phenols", 
    "Flavanoids",
    "Nonflavanoid phenols", 
    "Proanthocyanins",
    "Color intensity", 
    "Hue",
    "OD280/OD315 of diluted wines", 
    "Proline"
]]
y = data["Class"]

result = []

for k in range(1,51):
    clf = KNeighborsClassifier(n_neighbors=k)
    cv = KFold(len(y), n_folds=5, shuffle=True, random_state=42)
    cvs = cross_val_score(clf, X, y, cv=cv)
    result.append(np.mean(cvs))
    

bestScore = max(result)
bestScoreK = result.index(bestScore) + 1

print '1. Best score and K'
print '  ', "%.2f" % bestScore, bestScoreK

Xnorm = scale(X)

result = []

for k in range(1,51):
    clf = KNeighborsClassifier(n_neighbors=k)
    cv = KFold(len(y), n_folds=5, shuffle=True, random_state=42)
    cvs = cross_val_score(clf, Xnorm, y, cv=cv)
    result.append(np.mean(cvs))
    

bestScoreA = max(result)
bestScoreAK = result.index(bestScoreA) + 1

print '2. Best score and K after scale'
print '  ', "%.2f" % bestScoreA, bestScoreAK

out=open('../output/03/3.1.txt', 'w+')
print >>out, bestScoreK,
out=open('../output/03/3.2.txt', 'w+')
print >>out, "%.2f" % bestScore,
out=open('../output/03/3.3.txt', 'w+')
print >>out, bestScoreAK,
out=open('../output/03/3.4.txt', 'w+')
print >>out, "%.2f" % bestScoreA,