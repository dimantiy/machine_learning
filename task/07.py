print '# Task 07. SVN. Text.'
print

import numpy as np
from sklearn import datasets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import KFold

print '  ', 'Import completed'

newsgroups = datasets.fetch_20newsgroups(
                    subset='all', 
                    categories=['alt.atheism', 'sci.space']
             )

print '  ', 'Data loaded'

vectorizer = TfidfVectorizer()

X = vectorizer.fit_transform(newsgroups.data)
y = newsgroups.target

grid = { 'C': np.power(10.0, np.arange(-5, 6)) }
cv = KFold(y.size, n_folds=5, shuffle=True, random_state=241)
clf = SVC(kernel='linear', random_state=241)
gs = GridSearchCV(clf, grid, scoring='accuracy', cv=cv)
gs.fit(X, y)

print '  ', 'Folding completed'
print
print 'Scores:'

i = 0
score = 0;
parameters = 0;
for a in gs.grid_scores_:
    print '  ', a.mean_validation_score, a.parameters
    if i == 0 or a.mean_validation_score > score:
        score = a.mean_validation_score
        parameters = a.parameters
    i += 1

clf = SVC(kernel='linear', random_state=241, C=parameters['C'])
clf.fit(X, y)

coef = np.abs(clf.coef_.toarray()[0,:])
topIndex = coef.argsort()[-10:][::-1]

print
print 'Words:'
feature_mapping = vectorizer.get_feature_names()

words = []
for i in topIndex:
    words.append(feature_mapping[i])

words.sort()
out=open('../output/07/7.1.txt', 'w+')
print >>out, ",".join([str(i) for i in words]),
print '  ', ",".join([str(i) for i in words])
