print '# Task 01. Titanic'
print

import numpy as np
import pandas as pd
data = pd.read_csv('../data/titanic.csv', index_col='PassengerId')

print '1. Sex'
out=open('../output/01/1.1.txt', 'w+')
sex = data['Sex'].value_counts()
sexMale = sex['male'];
sexFemale = sex['female'];
print >>out, sexMale, sexFemale,
print '  ', sexMale, sexFemale
print

print '2. Survived'
out=open('../output/01/1.2.txt', 'w+')
survived = data['Survived'].value_counts()
survivedValue = 100.0 * survived[1] / len(data)
print >>out, "%.2f" % survivedValue,
print '  ', "%.2f" % survivedValue
print

print '3. Pclass'
out=open('../output/01/1.3.txt', 'w+')
pclass = data['Pclass'].value_counts()
pclassValue = 100.0 * pclass[1] / len(data)
print >>out, "%.2f" % pclassValue,
print '  ', "%.2f" % pclassValue
print

print '4. Age'
out=open('../output/01/1.4.txt', 'w+')
ageMean = data['Age'].mean()
ageMedian = data['Age'].median()
print >>out, "%.2f" % ageMean, "%.2f" % ageMedian,
print '  ', "%.2f" % ageMean, "%.2f" % ageMedian
print

print '5. Correlation'
out=open('../output/01/1.5.txt', 'w+')
df = pd.DataFrame({'A': data['SibSp'], 'B': data['Parch']})
corr = df.corr('pearson')['A']['B']
print >>out, "%.2f" % corr,
print '  ', "%.2f" % corr
print

print '6. Name'
out=open('../output/01/1.6.txt', 'w+')

def extractFirstName(fullName):
    name = fullName.split('. ')[1];
    if name.find('(') >= 0:
        name = name.split('(')[1];
    return name.split(' ')[0].replace('(', '').replace(')', '').replace('"', '');
    
names = data['Name'][data['Sex'] == 'female'].apply(extractFirstName)
popularName = names.mode()[0]

print >>out, popularName,
print '  ', popularName
print names.value_counts()
