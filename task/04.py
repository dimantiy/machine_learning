print '# Task 04. Metric'
print

from sklearn.datasets import load_boston


from sklearn.neighbors import KNeighborsRegressor
from sklearn.cross_validation import KFold
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import scale

import numpy as np
# import pandas as pd
# import sklearn as sk
# from sklearn.tree import DecisionTreeClassifier

data = load_boston()
X = scale(data.data)
y = data.target

result=[]
parr = []

for p in np.linspace(1.0, 10.0, num=200):
    regr = KNeighborsRegressor(n_neighbors=5, weights='distance', metric='minkowski', p=p)
    cv = KFold(len(y), n_folds=5, shuffle=True, random_state=42)
    cvs = cross_val_score(regr, X, y, scoring='mean_squared_error', cv=cv)
    result.append(np.mean(cvs))
    parr.append(p)

bestScore = max(result)
bestIndex = result.index(bestScore)
bestP = parr[bestIndex]

print '1. Best score and p'
print '  ', "%.2f" % bestScore, "%.2f" % bestP

# print result

out=open('../output/04/4.1.txt', 'w+')
print >>out, "%.2f" % bestP,